<?php

require_once "vendor/autoload.php";

use Monolog\Logger;
use Cherkizovo\KAM\Model\Category;

use util\Connection;
use util\CreateDatabase;

$log = new Logger('console');
$log->info("Parsing started");

CreateDatabase::createDatabase();

$xmlFileName = 'data/market.xml';

$import = new Import($xmlFileName);
$import->import();
