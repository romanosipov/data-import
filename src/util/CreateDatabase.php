<?php

namespace util;

class CreateDatabase {
  public static function createDatabase() {
    $db = DBConnection::getConnection();

    $sql = <<<EOF
      CREATE TABLE CATEGORY (
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        CODE INT NOT NULL UNIQUE,
        NAME VARCHAR(256) NOT NULL,
        COLOR VARCHAR(256));
EOF;

    $db->exec($sql);

    $sql = <<<EOF
      CREATE TABLE PARTNER (
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        CODE INT NOT NULL UNIQUE,
        NAME VARCHAR(256) NOT NULL);
EOF;

    $db->exec($sql);

    $sql = <<<EOF
      CREATE TABLE SUPPLIER (
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        CODE INT NOT NULL UNIQUE,
        NAME VARCHAR(256) NOT NULL,
        COLOR VARCHAR(256));
EOF;

    $db->exec($sql);

    $sql = <<<EOF
      CREATE TABLE ITEM (
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        ITEM_DATE TEXT,
        SUPPLIER_ID INTEGER NOT_NULL,
        PARTNER_ID INTEGER NOT NULL,
        CATEGORY_ID INTEGER NOT NULL,
        REGION_ID INTEGER NOT NULL,
        RUB_TOTAL DECIMAL(10,2),
        RUB_OWN_TM DECIMAL(10,2),
        TON_TOTAL DECIMAL(10,3),
        TON_OWN_TM DECIMAL(10,3));
EOF;

    $db->exec($sql);
  }
}
