<?php

namespace util;

use Monolog\Logger;
use SQLite3;

class DBConnection {
  private static $db;
  private static $dbName = "market.sqlite";

  public static function getConnection() {
    if (is_null(DBConnection::$db)) {
      self::$db = new SQLite3(DBConnection::$dbName);
    }

    return DBConnection::$db;
  }
}
