<?php

namespace Cherkizovo\KAM\Model;

use Exception;
use Monolog\Logger;
use util\DBConnection;

class lm_basic_model {
  protected $id;
  protected $logger;

  protected static $INSERT_STMT = "";

  public function __construct() {
    $this->logger = new Logger('console');
  }

  protected function populateStatement($stmt) {}

  public function save() {
    $db = DBConnection::getConnection();

    $stmt = $db->prepare(static::$INSERT_STMT);
    $this->populateStatement($stmt);

    $result = $stmt->execute();

    $this->id = $db->lastInsertRowID();
  }

  function __get($name) {
    return $this->$name;
  }

  function __set($name, $value) {
    if ($name == "id") {
      throw new Exception('Property named ID cannot be set.');
    }
  }

  function __toString() {
    return "lm_basic_model: {id: " . $this->id . "}";
  }
}
