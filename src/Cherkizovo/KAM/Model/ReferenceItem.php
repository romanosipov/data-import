<?php

namespace Cherkizovo\KAM\Model;

use util\DBConnection;

class ReferenceItem extends lm_basic_model {
    public $code;
    protected static $GET_BY_CODE_STMT = "";

    protected static function create($stmtResult) {}

    public static function getByCode($code) {
      $item = null;
      $db = DBConnection::getConnection();

      $stmt = $db->prepare(static::$GET_BY_CODE_STMT);
      $stmt->bindValue(":code", $code);

      $result = $stmt->execute()->fetchArray(SQLITE3_ASSOC);
      if (is_array($result)) {
        $item = static::create($result);
      }

      return $item;
    }
}
