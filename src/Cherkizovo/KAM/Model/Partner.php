<?php

namespace Cherkizovo\KAM\Model;

class Partner extends ReferenceItem {
  public $name;
  protected static $INSERT_STMT = "INSERT INTO PARTNER(code, name) VALUES(:code, :name)";
  protected static $GET_BY_CODE_STMT = "SELECT id, code, name FROM PARTNER where code = :code";

  protected function populateStatement($stmt) {
    $stmt->bindValue(":code", $this->code, SQLITE3_INTEGER);
    $stmt->bindValue(":name", $this->name, SQLITE3_TEXT);
  }

  public function __toString() {
    return "Partner: {id: " . $this->id . ", code: " . $this->code . ", name: " . $this->name . "}";
  }

  protected static function create($columns) {
    $item = new self();

    $item->id = $columns["ID"];
    $item->code = $columns["CODE"];
    $item->name = $columns["NAME"];

    return $item;
  }
}
