<?php

namespace Cherkizovo\KAM\Model;

class Item extends lm_basic_model {
  public $date;
  public $supplierId;
  public $partnerId;
  public $categoryId;
  public $regionId;
  public $rubTotal;
  public $rubOwnTM;
  public $tonTotal;
  public $tonOwnTM;

  protected static $INSERT_STMT = "INSERT INTO ITEM(item_date, supplier_id, partner_id, category_id, region_id, rub_total, rub_own_tm, ton_total, ton_own_tm) VALUES(:date, :supplierId, :partnerId, :categoryId, :regionId, :rubTotal, :rubOwnTM, :tonTotal, :tonOwnTM)";

  protected function populateStatement($stmt) {
    $stmt->bindValue(":date", $this->date, SQLITE3_TEXT);
    $stmt->bindValue(":supplierId", $this->supplierId, SQLITE3_INTEGER);
    $stmt->bindValue(":partnerId", $this->partnerId, SQLITE3_INTEGER);
    $stmt->bindValue(":categoryId", $this->categoryId, SQLITE3_INTEGER);
    $stmt->bindValue(":regionId", $this->regionId, SQLITE3_INTEGER);
    $stmt->bindValue(":rubTotal", $this->rubTotal, SQLITE3_FLOAT);
    $stmt->bindValue(":rubOwnTM", $this->rubOwnTM, SQLITE3_FLOAT);
    $stmt->bindValue(":tonTotal", $this->tonTotal, SQLITE3_FLOAT);
    $stmt->bindValue(":tonOwnTM", $this->tonOwnTM, SQLITE3_FLOAT);
  }

  public function __toString() {
    return "Item: {id: " . $this->id . ", date: " . $this->date . ", supplierId: " . $this->supplierId . "}";
  }

  protected static function create($columns) {
    $item = new self();

    $item->id = $columns["ID"];
    $item->code = $columns["CODE"];
    $item->name = $columns["NAME"];

    return $item;
  }

  protected function getCategoryResult($regionId, $partnerId, unit) {
    // TODO Не требуется в соответствии с условиями задачи
  }

  protected function getSupplierResult($regionId, $partnerId, unit) {
    // TODO Не требуется в соответствии с условиями задачи
  }
}
