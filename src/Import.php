<?php

use Monolog\Logger;
use Cherkizovo\KAM\Model\Category;
use Cherkizovo\KAM\Model\Partner;
use Cherkizovo\KAM\Model\Supplier;
use Cherkizovo\KAM\Model\Item;

class Import {
  private $logger;
  private $market;

  public function __construct($file) {
    $this->logger = new Logger('console');
    $this->market = simplexml_load_file($file);
  }

  private function populateCategory($item) {
    $category = new Category();
    $category->code = intval($item['id']->__toString());
    $category->name = $item->__toString();

    return $category;
  }

  private function populatePartner($item) {
    $partner = new Partner();
    $partner->code = intval($item['id']->__toString());
    $partner->name = $item->name[0]->__toString();

    return $partner;
  }

  private function populateSupplier($item) {
    $supplier = new Supplier();
    $supplier->code = intval($item['id']->__toString());
    $supplier->name = $item->name[0]->__toString();

    return $supplier;
  }

  private function importReferenceItems($items, $entityClass, $popFunc) {
    $this->logger->info("importReferenceItems");

    foreach ($items as $item) {
      $storedItem = $entityClass::getByCode(intval($item['id']->__toString()));
      $this->logger->info("Existing item: $storedItem");
      if (is_null($storedItem)) {
        $this->logger->info("Item does not exists");
        $itemToStore = $popFunc($item);
        $this->logger->info("Item before saving: $itemToStore");
        $itemToStore->save();
        $this->logger->info("Item after saving: $itemToStore");
      } else {
        $this->logger->info("Item exists and will not load");
      }
    }
  }

  private function importItems() {
    $this->logger->info("Importing items");
    $date = $this->market['date'];

    foreach ($this->market->supplier as $supplier) {
      $supplierId = intval($supplier['id']);

      foreach ($supplier->region as $region) {
        $regionId = intval($region['id']);

        foreach ($region->partner as $partner) {
          $partnerId = intval($partner['id']);

          foreach($partner->category as $category) {
            $categoryId = intval($category['id']);

            $rubTotal = floatval($category->rub[0]->total[0]);
            $rubOwnTM = floatval($category->rub[0]->ownTM[0]);
            $tonTotal = floatval($category->ton[0]->total[0]);
            $tonOwnTM = floatval($category->ton[0]->ownTM[0]);

            $item = new Item();
            $item->date = $date;
            $item->supplierId = $supplierId;
            $item->regionId  = $regionId;
            $item->partnerId = $partnerId;
            $item->categoryId = $categoryId;
            $item->rubTotal = $rubTotal;
            $item->rubOwnTM = $rubOwnTM;
            $item->tonTotal = $tonTotal;
            $item->tonOwnTM = $tonOwnTM;
            $item->save();
          }
        }
      }
    }
  }

  public function import() {
    $this->logger->info("Importing categories");
    $this->importReferenceItems($this->market->categories[0]->category, Category::class, function($item) {return $this->populateCategory($item);});

    $this->logger->info("Importing partners");
    $this->importReferenceItems($this->market->partners[0]->partner, Partner::class, function($item) {return $this->populatePartner($item);});

    $this->logger->info("Importing suppliers");
    $this->importReferenceItems($this->market->supplier, Supplier::class, function($item) {return $this->populateSupplier($item);});

    $this->logger->info("Importing items");
    $this->importItems();
  }

}
